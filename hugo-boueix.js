const name = "hugo-boueix"
const promo = "B2A"

const q1 = `
SELECT Name
FROM Track
WHERE Milliseconds < 
  (SELECT Milliseconds 
  FROM Track 
  WHERE TrackId = 3457)
`
const q2 = `
SELECT Name
FROM Track
WHERE MediaTypeId = 
  (SELECT MediaTypeId
  FROM Track
  WHERE Name = 'Rehab')
`
const q3 = `
SELECT Playlist.PlaylistId, 
  Playlist.Name,
  COUNT(PlaylistTrack.TrackId) as NbTrack, 
  ISNULL(SUM(Track.Milliseconds), 0) as DuréeTotale,
  ISNULL(SUM(Track.Milliseconds) / NULLIF(COUNT(PlaylistTrack.TrackId), 0), 0) as DuréeMoyTrack
FROM PlaylistTrack
FULL JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
FULL JOIN Playlist ON PlaylistTrack.PlaylistId = Playlist.PlaylistId
GROUP BY Playlist.PlaylistId, Playlist.Name
ORDER BY Playlist.PlaylistId
`
const q4 = `
SELECT Playlist.PlaylistId, 
  Playlist.Name
FROM PlaylistTrack
FULL JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
FULL JOIN Playlist ON PlaylistTrack.PlaylistId = Playlist.PlaylistId
GROUP BY Playlist.PlaylistId, Playlist.Name
HAVING ISNULL(SUM(Track.Milliseconds), 0) > 
(
  SELECT SUM(CAST(tab1.DuréeTotale as bigint)) / COUNT(tab1.PlaylistId) as DuréeMoyPlaylist
  FROM (
  SELECT Playlist.PlaylistId, 
    Playlist.Name,
    ISNULL(SUM(Track.Milliseconds), 0) as DuréeTotale
  FROM PlaylistTrack
  FULL JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
  FULL JOIN Playlist ON PlaylistTrack.PlaylistId = Playlist.PlaylistId
  GROUP BY Playlist.PlaylistId, Playlist.Name
  ) as tab1
)
ORDER BY Playlist.PlaylistId
`
const q5 = `
SELECT Playlist.PlaylistId, Playlist.Name
FROM Playlist
JOIN PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
GROUP BY Playlist.PlaylistId, Playlist.Name
HAVING COUNT(PlaylistTrack.TrackId) IN (
  SELECT COUNT(PlaylistTrack.TrackId) as NbTrack
  FROM PlaylistTrack
  WHERE PlaylistTrack.PlaylistId = 1
  UNION
  SELECT COUNT(PlaylistTrack.TrackId) as NbTrack
  FROM PlaylistTrack
  WHERE PlaylistTrack.PlaylistId = 13
)
`
const q6 = `
SELECT DISTINCT Customer.CustomerId, Customer.FirstName, Customer.LastName
FROM Invoice
JOIN Customer ON Customer.CustomerId = Invoice.CustomerId
WHERE Invoice.Total > (
  SELECT MAX(Total)
  FROM Invoice
  WHERE BillingCountry = 'France'
) AND BillingCountry <> 'France'
`
const q7 = `
SELECT Invoice.BillingCountry, 
  MIN(Invoice.Total) AS MiniInvoice, 
  MAX(Invoice.Total) AS MaxiInvoice, (
    SELECT COUNT(Invoice.Total) / COUNT(DISTINCT Invoice.BillingCountry) 
    FROM Invoice
  ) AS NbMoyInvoice,
  COUNT(Invoice.Total) AS NbTotal,
  CAST(COUNT(Invoice.Total) AS FLOAT) / 
  CAST((
    SELECT COUNT(Invoice.Total) / COUNT(DISTINCT Invoice.BillingCountry) 
    FROM Invoice
  ) AS FLOAT) * 100 - 100 AS NbProgression,
  CAST(SUM(Invoice.Total) AS FLOAT) / 
  CAST((
    SELECT SUM(Invoice.Total) / COUNT(DISTINCT Invoice.BillingCountry) 
    FROM Invoice
  ) AS FLOAT) * 100 - 100 AS PriceProgression
FROM Invoice
GROUP BY Invoice.BillingCountry
`
const q8 = `
SELECT Track.TrackId, 
  Track.Name, 
  Track.Composer, 
  Track.Milliseconds, 
  Track.Bytes, 
  Track.UnitPrice, 
  MediaType.MediaTypeId, 
  MediaType.Name AS NameMedia,
  (SELECT ROUND(AVG(UnitPrice), 2) FROM Track) AS PrixMoyGlobal, 
  ROUND(AVG(UnitPrice), 2) AS PrixMoyMedia
FROM Track
JOIN MediaType ON MediaType.MediaTypeId = Track.MediaTypeId
WHERE unitprice > (SELECT AVG(UnitPrice) FROM Track)
GROUP BY  Track.TrackId, Track.Name, Track.Composer, Track.Milliseconds, Track.Bytes, Track.UnitPrice, MediaType.MediaTypeId, MediaType.Name
`
const q9 = ``
const q10 = ``
const q11 = ``
const q12 = ``
const q13 = ``
const q14 = ``
const q15 = ``
const q16 = `
SELECT TOP 1 Employee.LastName, Employee.FirstName
FROM Employee
INNER JOIN Customer ON Employee.EmployeeId = Customer.SupportRepId
INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId
GROUP BY Employee.FirstName, Employee.LastName
ORDER BY SUM(Invoice.Total)
`
const q17 = ``
const q18 = `
DROP DATABASE IF EXISTS [part2];
GO
CREATE DATABASE [part2];
GO
USE [part2];
GO

CREATE TABLE [dbo].[Group]
(
  [id] INT NOT NULL IDENTITY,
  [name] NVARCHAR(40) NOT NULL,
  [display_name] NVARCHAR(40) NOT NULL,
  [description] NVARCHAR(255),
  CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED ([id])

);
GO
CREATE TABLE [dbo].[User]
(
  [id] INT NOT NULL IDENTITY,
  [username] NVARCHAR(40) NOT NULL,
  [email] NVARCHAR(50) NOT NULL,
  [superuser] BIT NOT NULL,
  CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([id])
);
GO
CREATE TABLE [dbo].[Role]
(
  [id] INT NOT NULL IDENTITY,
  [name] NVARCHAR(40) NOT NULL,
  [display_name] NVARCHAR(40) NOT NULL,
  [description] NVARCHAR(255),
  CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([id])
);
GO
CREATE TABLE [dbo].[Permission]
(
  [id] INT NOT NULL IDENTITY,
  [name] NVARCHAR(40) NOT NULL,
  [display_name] NVARCHAR(40) NOT NULL,
  [description] NVARCHAR(255),
  CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED ([id])
);
GO
CREATE TABLE [dbo].[User_Group]
(
  [user_id] INT NOT NULL,
  [group_id] INT NOT NULL
);
GO
CREATE TABLE [dbo].[Group_Role]
(
  [group_id] INT NOT NULL,
  [role_id] INT NOT NULL
);
GO
CREATE TABLE [dbo].[User_Role]
(
  [user_id] INT NOT NULL,
  [role_id] INT NOT NULL
);
GO
CREATE TABLE [dbo].[Role_Permission]
(
  [role_id] INT NOT NULL,
  [permission_id] INT NOT NULL
);
GO

ALTER TABLE [dbo].[User_Group] ADD CONSTRAINT [FK_User_Group_user_id]
    FOREIGN KEY ([user_id]) REFERENCES [dbo].[User] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [dbo].[User_Group] ADD CONSTRAINT [FK_User_Group_group_id]
    FOREIGN KEY ([group_id]) REFERENCES [dbo].[Group] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [dbo].[Group_Role] ADD CONSTRAINT [FK_Group_Role_group_id]
    FOREIGN KEY ([group_id]) REFERENCES [dbo].[Group] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [dbo].[Group_Role] ADD CONSTRAINT [FK_Group_Role_user_id]
    FOREIGN KEY ([role_id]) REFERENCES [dbo].[Role] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [dbo].[Role_Permission] ADD CONSTRAINT [FK_Role_Permission_user_id]
    FOREIGN KEY ([role_id]) REFERENCES [dbo].[Role] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [dbo].[Role_Permission] ADD CONSTRAINT [FK_Role_Permission_group_id]
    FOREIGN KEY ([permission_id]) REFERENCES [dbo].[Permission] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [dbo].[User_Role] ADD CONSTRAINT [FK_User_Role_group_id]
    FOREIGN KEY ([user_id]) REFERENCES [dbo].[User] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [dbo].[User_Role] ADD CONSTRAINT [FK_User_Role_user_id]
    FOREIGN KEY ([role_id]) REFERENCES [dbo].[Role] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
`
const q19 = `
INSERT INTO Track 
  (Name, 
  AlbumId, 
  MediaTypeId, 
  GenreId, 
  Composer, 
  Milliseconds, 
  Bytes, 
  UnitPrice) 
VALUES 
  ('New1', 10, 1, 1, 'Test', 33800, 2048, 0.99), 
  ('New2', 10, 1, 1, 'Test', 33800, 2048, 0.99), 
  ('New3', 10, 1, 1, 'Test', 33800, 2048, 0.99);
`
const q20 = `
INSERT INTO Employee 
  (LastName, 
  FirstName, 
  Title, 
  ReportsTo,
  BirthDate, 
  HireDate, 
  Address, 
  City, 
  State, 
  Country, 
  PostalCode, 
  Phone, 
  Fax, 
  Email) 
VALUES 
  ('Boueix', 'Hugo', 'Boss', 1, 19999-05-04, 2018-09-08, '89 Quais des Chartrons', 'Bordeaux', 'FR', 'France', '33800', '0600000000', '0000000000', 'hugo.boueix@ynov.com'), 
  ('Test', 'EncoreTest', 'Stagiaire', 1, 1420-12-30, 2018-09-08, '89 Quais des Chartrons', 'Bordeaux', 'FR', 'France', '33800', '0600000001', '0000000001', 'test@ynov.com')
`
const q21 = `
DELETE FROM InvoiceLine
WHERE InvoiceId IN (
  SELECT InvoiceId 
  FROM Invoice 
  WHERE YEAR(InvoiceDate) = 2010
)
DELETE FROM Invoice
WHERE YEAR(InvoiceDate) = 2010
`
const q22 = `
UPDATE Invoice
SET CustomerId = (
  SELECT TOP 1 Customer.CustomerId
  FROM Customer
  JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId
  WHERE Customer.Country = 'France'
  GROUP BY Customer.CustomerId
  ORDER BY COUNT(Invoice.InvoiceId) DESC
)
WHERE YEAR(InvoiceDate) >= 2011 
  AND YEAR(InvoiceDate) <= 2014
  AND BillingCountry = 'Germany'
`
const q23 = `
UPDATE Invoice
SET Invoice.BillingCountry = Customer.Country
FROM Invoice
JOIN Customer ON Invoice.CustomerId = Customer.CustomerId
WHERE Invoice.BillingCountry <> Customer.Country
`
const q24 = `
ALTER TABLE Employee 
ADD Salary int
`
const q25 = `
UPDATE Employee 
SET Salary = ROUND(RAND(CHECKSUM(NEWID()))*(100000-30000), 0) + 30000
`
const q26 = `
UPDATE Employee 
SET Salary = ROUND(RAND(CHECKSUM(NEWID()))*(100000-30000), 0) + 30000
`











































// NE PAS TOUCHER CETTE SECTION
const tp = {name: name, promo: promo, queries: [q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20, q21, q22, q23, q24, q25, q26]}
module.exports = tp
